#-*- coding: utf-8-
from odoo import fields, models, api
from odoo.exceptions import Warning 

class Book(models.Model):
    _name = 'library.book'
    
    name = fields.Char('Title', required=True)
    isbn = fields.Char('ISBN')
    active = fields.Boolean('Active?', default=True)
    date_published = fields.Date()
    image = fields.Binary('Cover')
    publisher_id = fields.Many2one('res.partner', string='Publisher')
    author_ids = fields.Many2many('res.partner', string='Authors')

# decorator
# http://odoo-new-api-guide-line.readthedocs.org/en/latest/decorator.html
# sejak odoo 13 all the decorators @api.multi, @api.returns, @api.one, @api.cr, @api.model_cr dihapus

    def _check_isbn(self):
        # edu = self.env['library.book']
        # test = edu.search([])
        # print("")
        # print(test)
        # print("")
        is_pass = False
        if len(self.isbn) == 13:
            is_pass = True
        else:
            is_pass = False
        return is_pass

    def button_check_isbn(self):
        for books in self:
            if not books.isbn:
                raise Warning('Please provide an ISBN for %s' % books.name)
            if books.isbn and not books._check_isbn():
                raise Warning('%s is an invalid ISBN' % books.isbn)
        return True