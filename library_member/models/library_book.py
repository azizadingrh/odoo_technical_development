from odoo import fields, models

class Book(models.Model):
    _inherit = 'library.book'

    is_available = fields.Boolean('Is Available?')
    isbn = fields.Char(help="Use a valid ISBN-13 or ISBN-10.")

    def _check_len_isbn(self):
        is_pass = False
        if len(self.isbn) == 10:
            is_pass = True
            return is_pass
        else:
            return super()._check_isbn()

